# holiday-emojis

As part of a project to increase the amount of colour on my website, I put together this quick and silly program to output a different emoji depending on the current holiday.

## Preloaded Holidays

The following holidays are included by default, but you can add your own:

* Easter - Hatching Chick :hatching_chick:
* Halloween - Pumpkin Lantern :jack_o_lantern:
* Christmas - Christmas Tree :christmas_tree:
* New Year - Fireworks :fireworks:

## Adding Your Own Holidays

Edit the ```holiday-emojis.php``` file and add elements to the ```$cal``` array at the top. You can use the preloaded examples to understand the syntax.

The ```from``` and ```to``` values must be in the format ```DD ShortMonth```. For example: ```01 Jan```, ```05 May```, ```20 Jun``` and ```28 Dec```.

If you want a time range to span into the next year, simply add ```next year``` to the end. For example ```10 Jan next year```.

The ```output``` value can contain anything - it doesn't *have* to be an emoji.

## Previewing

The output of ```debug.php``` shows all loaded outputs at once. By default, this requires your PHP ```include_path``` to be set to the same directory as the ```holiday-emojis.php``` file.
